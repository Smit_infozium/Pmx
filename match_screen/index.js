const express = require('express');
const axios = require('axios');
const cors = require('cors');
const request = require('request');
const mysql = require('mysql');
const redis = require('redis');


const client = redis.createClient(6379);


client.on('error', (err)=>{
    console.log("Error::=>", err);
});

const config = mysql.createConnection({
    connectionLimit:10,
    host:'localhost',
    user:'root',
    password:"",
    database:'upcoming_db_master',
    multipleStatements: true
});

config.connect(function(error) {
    if (error) throw error;
    console.log("connected")
});


const app = express();
const router = express.Router();
app.use(express.json());
app.use(cors());


//upcoming match list from master database
app.post('/upcoming/cricket-match',
        async (req, resp) => {
            console.log("IN UPCOMING");

            config.query("SELECT * FROM upcoming", function(error, result, fields) {
                if(error) throw error;
                resp.send({tiles:JSON.parse(JSON.stringify(result))});
                 
            });
        }
)

// upcoming match list using mysql view
app.post('/cricket-match/view/upcoming', async (req, res)=>{

    console.log("In Upcoming View");

    config.query("SELECT * FROM upcoming_view", function(error, result, fields){
        if(error) throw error;
        res.status(200).send({tiles:JSON.parse(JSON.stringify(result))});
    });
});

// Fetching live match list using mysql view
app.post('/cricket-match/view/live', async (req, res)=>{
    console.log("In Live View");

    config.query("SELECT * FROM live_view", async (error, result, fields) => {
        if(error) throw error;
        res.status(200).send({tiles:JSON.parse(JSON.stringify(result))});
    })

});

// Finished match data using mysql view
app.post('/cricket-match/view/completed', async (req, res) =>{
    console.log("In Completed View");

    config.query("SELECT * FROM completed_view", async(error, results, fields) =>{
        if(error) throw error;
        res.status(200).send({tiles:JSON.parse(JSON.stringify(results))})
    });
});

// Finished match list using redis caching and mysql view
app.post('/cricket-match/cache/completed', async (req, res)=>{
        const cachedUpcoming = 'completed:matches';

        return client.get(cachedUpcoming, (err, result)=>{
            if (result){
                return res.json({source:'cache', tiles:JSON.parse(result)})
            }
            if(!result){
                config.query("SELECT * FROM completed_view", async(error, results, fields) =>{
                    if(error) throw error;
                    client.set(cachedUpcoming, JSON.stringify(results))
                    res.status(200).send({source:'api',tiles:JSON.parse(JSON.stringify(results))})
                });
            }
        });
});

app.listen(8000, () =>{
        console.log("Listening on 8000");
});
