const express = require('express');
const axios = require('axios');
const mysql = require('mysql');
const cors = require('cors');
const  request  = require('request');
const fs = require('fs');

const conn = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'commentary',
    multipleStatements: true
})

conn.connect((err) => {
    if(err) throw err;
    console.log("connected to 'commentary' database");
});

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors());


// reading from file-commentry
app.post('/save/match/:match_id/innings/:ing/commentry/file', async (req, res) => {
    mid = req.params.match_id;
    inn = req.params.ing;

    fs.readFile('./for_commentry.json', function(err, data){
        data = JSON.parse(data);
        answer = data.response;
        // const comm = JSON.stringify(answer.commentaries);
        let sql = `SELECT * FROM commentaries WHERE match_id=${mid} AND inning=${inn}`
        conn.query(sql, function(err, result){
            
            if(result.length == 0){
                sql = `INSERT INTO commentaries (match_id, inning, commentary) VALUES (?)`;
                conn.query(sql, [[mid, inn, JSON.stringify(answer.commentaries)]], function(err, results){
                    if(err) throw err;
                    console.log("ROWS INSERTED", results.affectedRows);
                })
            }else{
                console.log(false);
                sql = `UPDATE commentaries SET commentary=? WHERE match_id=${mid} AND inning=${inn}`
                conn.query(sql,[JSON.stringify(answer.commentaries)], function(err, result){
                    if(err) throw err;
                    console.log("ROW Updated:", result.affectedRows);
                })
            }
        })

        res.send({stauts:"OK"});
        
    });
    // console.log(data);
});


//localhost:8007/retrieve/match/mid/
app.post('/retrieve/match/:mid/inn/:ing/commentary', async (req, res) =>{
        mid = req.params.mid;
        inn = req.params.ing;
        const sql = `SELECT * FROM read_commentaries WHERE match_id=${mid} AND inning=${inn}`;
        conn.query(sql, function(error, result){
            if(error) throw error;
            result[0].commentaries = JSON.parse(result[0].commentary);
            delete result[0].commentary;
            // console.log(typeof(result[0].match_id));
            res.send(result);
        });

});


// API TRIAL ENDED- LEFT IT
app.post('/save/match/:match_id/innings/:ing/commentry', async (req, resp) => {
    mid = req.params.match_id;
    inn = req.params.ing;

     request.get({url: `https://rest.entitysport.com/v2/matches/${mid}/innings/${inn}/commentary?token=0401628019fa4df1522947816389fdf6`, json:true}, function(err, res, data){
        if(err) throw err;
        answer = data.response;
        const comm = answer.commentaries;
        console.log(answer);
        let sql = `SELECT * FROM commentaries WHERE match_id=${mid} AND inning=${inn}`
        conn.query(sql, function(err, result){
            
            if(result.length == 0){
                sql = `INSERT INTO commentaries (match_id, inning, commentary) VALUES (?)`;
                conn.query(sql, [[mid, inn, JSON.stringify(answer.commentaries)]], function(err, result){
                    if(err) throw err;
                    console.log("ROWS INSERTED", result[0].affectedRows);
                })
            }else{
                console.log(false);
            }
        })

        console.log(answer.commentaries.length);
        resp.send({});
    })
});


app.listen(8007, ()=>{
    console.log("Listening on Port '8007'")
});