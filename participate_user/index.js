const express = require('express');
const axios = require('axios');
const cors = require('cors');
const mysql = require('mysql');
const {Kafka, logLevel} = require('kafkajs');


const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'participate_user',
    multipleStatements: true
})

conn.connect((err) => {
    if (err) throw (err);
    console.log('CONNECTED TO participate_user');
});

const kafka = new Kafka({
    clientId:'receiver',
    brokers:['localhost:9092', 'localhost:9093', 'localhost:9094'],
    // loglevel: logLevel.ERROR
})

const consumer = kafka.consumer({groupId:'userteam', allowAutoTopicCreation:false});
consumer.connect().then(()=>console.log("Connected to the Broker"), err=>console.log(err));
consumer.subscribe({topic:'UserCreatedTeam'}).then(()=>console.log(`Subscribed to UserCreatedTeam`));

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// localhost:8010/user/2fe6fd81/participate/match/48132/contest/430/team/17_284_4_1fbed454-e678-4bb3-9342-8da38d7383af
app.post('/user/:uid/participate/match/:mid/contest/:cid/team/:tid', async (req, res) => {
    const uid = req.params.uid;
    const mid = req.params.mid;
    const cid = req.params.cid;
    const tid = req.params.tid;

    const data = req.body;
    const contest_price = data.contest_amount;
    const team = data.team;
    const captain = data.captain;
    const vice_captain = data.vice_captain;
    const jackpot = data.jackpot;
    const arr = [uid, mid, cid, tid, team, captain, vice_captain, jackpot, contest_price];

    // console.log(arr);

    let sql = 'INSERT INTO user_contest (user_id, match_id, contest_id, team_id, team, captain, vice_captain, jackpot, contest_amount) VALUES (?)'
    conn.query(sql, [[uid, mid, cid, tid, JSON.stringify(team), captain, vice_captain, jackpot, contest_price]], (err, results) => {
        if (err) throw err;
        console.log("ROWS AFFECTED:", results.affectedRows);

        axios.post('http://localhost:11000/events', {type:'UserParticipated', data:{user_id:uid}});
        res.status(201).send({ arr });
    });
});



app.post('/get/user/:uid/participation/match/:mid', async (req, res) => {
    const uid = req.params.uid;
    const mid = req.params.mid;

    let sql = 'SELECT * FROM user_contest WHERE user_id=?, match_id=?'
    conn.query(sql, [uid, mid], (err, results) => {
        if (err) throw err;
        res.status(200).send({ result });

    })

});



//compare/teams/17_284_4_1fbed454-e678-4bb3-9342-8da38d7384sf/17_284_4_1fbed454-e678-4bb3-9342-8da38d7383af/users/2fe6fd81
app.post('/compare/teams/:tid1/:tid2/users/:uid1/:uid2', async (req, res) => {                                    //:uid1/:uid2
    tid1 = req.params.tid1;
    tid2 = req.params.tid2;
    uid1 = req.params.uid1;
    uid2 = req.params.uid2;

    let sql = 'SELECT team, captain, vice_captain, jackpot FROM user_contest WHERE team_id =? OR team_id =?';
    const arr = [tid1, tid2];
    conn.query(sql, [tid1, tid2], function (err, results) {
        if (err) throw err;
        t1 = results[0];
        t1team = JSON.parse(t1.team);
        t1cap = t1.captain;
        t1vcap = t1.vice_captain;
        t1jackpot = t1.jackpot;

        t2 = results[1];
        t2team = JSON.parse(t2.team);
        t2cap = t2.captain;
        t2vcap = t2.vice_captain;
        t2jackpot = t2.jackpot;

        same = []
        diff1 = []
        diff2 = []
        c_vc_j = []
        c_vc_j_1 = []

        for (let index = 0; index < t1team.length; index++) {
            for (let jndex = 0; jndex < t2team.length; jndex++) {
                if (t1team[index].player_id == t2team[jndex].player_id) {
                    if (t1team[index].player_id == t1cap) {
                        t1team[index].dream_role = 'C';
                        c_vc_j.push(t1team[index]);
                        break;
                    }
                    if (t1team[index].player_id == t1vcap) {
                        t1team[index].dream_role = 'VC';
                        c_vc_j.push(t1team[index])
                        break;
                    }
                    if (t1team[index].player_id == t1jackpot) {
                        t1team[index].dream_role = 'J';
                        c_vc_j.push(t1team[index])
                        break;
                    }
                    same.push(t1team[index]);
                    break;
                } else {
                    if (jndex == t2team.length - 1) {
                        diff1.push(t1team[index])
                    }
                }
            }
        }
        for (let index = 0; index < t2team.length; index++) {
            for (let jndex = 0; jndex < t1team.length; jndex++) {
                if (t2team[index].player_id == t1team[jndex].player_id) {
                    if (t2team[index].player_id == t2cap) {
                        t2team[index].dream_role = 'C';
                        c_vc_j_1.push(t1team[index]);
                        break;
                    }
                    if (t2team[index].player_id == t2vcap) {
                        t2team[index].dream_role = 'VC';
                        c_vc_j_1.push(t1team[index])
                        break;
                    }
                    if (t2team[index].player_id == t2jackpot) {
                        t2team[index].dream_role = 'J';
                        c_vc_j_1.push(t1team[index])
                        break;
                    }
                    // same.push(t1team[index]);
                    break;
                } else {
                    if (jndex == t1team.length - 1) {
                        diff2.push(t2team[index])
                    }
                }
            }
        }
        console.log("SAME", same);
        console.log("C_VC_J_1", c_vc_j_1);
        console.log("C_VC_J", c_vc_j);

        res.send({ same, [uid1]: diff1, [uid2]: diff2, c_vc_j, c_vc_j_1 });
    });
})


app.post('/events', (req, res)=>{
    console.log("KAFKA EVENT IN PARTICIPATE USER SERVICE")
    // console.log(req.body);
    run().then(()=>console.log('Done'), err => console.log(err));

    async function run(){
        
        await consumer.run({
            eachMessage: async ({message}) =>{
                console.log(JSON.parse(message.value));
            }
        })
    }
})

app.listen(8010, () => {
    console.log('listening on port "8010" - service: participate_user');
});