const express = require('express');
const cors = require('cors');
const axios = require('axios');
const bodyParser = require('body-parser');
const {Kafka, logLevel, PartitionAssigners} = require('kafkajs');


const app = express();
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({extended:true}));

const kafka = new Kafka({
//     clientId:'receiver',
    brokers:['localhost:9092', 'localhost:9093', 'localhost:9094'],
    logLevel:logLevel.ERROR
});

const consumer = kafka.consumer({groupId:'upcoming', allowAutoTopicCreation:false});
consumer.connect().then(()=>console.log("connected"), err => console.log(err))
consumer.subscribe({topic: 'UpdatedUpcoming'}).then(()=>console.log("Connected to The Kafka Topic"));

consumer.on(consumer.events.HEARTBEAT, ()=>{
    console.log("HeartBeat Send")
})
consumer.on(consumer.events.FETCH_START, (error)=>{
    console.log("FETCHING STARTED")
})

// consumer.heartbeat();

const createContest = (match_id) => {
    contest = {};
    contest.contest_id = 1023;
    contest.entry_amount = 29;
    contest.prize_pool = 500000;
    contest.spot = 150000;
    contest.category = 'Mega';
    contest.percentage = 64;
    // new_obj = {`${id}`:[`${contest}`]}
    
     console.log(1,`${JSON.stringify(match_id)}:${JSON.stringify(contest)}`);
}


//Event Without Kafka
// app.post('/events', async (req, res) => {

//     const {type, match_ids} = req.body;
//     if(type == 'Upcoming-Updated'){
//         console.log(type, match_ids);
//         // console.log(result);

//         res.send({status:"OK"});
//     }
// })
app.post('/events', async (req, res) =>{
    console.log("KAFKA EVENT");
    run().then(()=> console.log('Done'), err => console.log(err));


    async function run(){
        // const kafka = new Kafka({brokers:['localhost:9092']});
    
        // const consumer = kafka.consumer({groupId:"upcoming", allowAutoTopicCreation:false});
    
        // await consumer.connect();
    
        // await consumer.subscribe({ topic:'UpdatedUpcoming', fromBeginning:true });
        await consumer.run({
            eachMessage: async({message}) => {
                console.log(JSON.parse(message.value));
            }
        });
    }
    
    // consumer.disconnect();
    // consumer.connect().then(console.log("Consumer Connected"));

    res.send({status:'OK'})

})

app.post('/create-contest/:id', async (req, res) => {
    const match_id = req.params.id;
    console.log(2,match_id);
    const result = createContest(match_id);
    // console.log(result);
    await axios.post('http://localhost:11000/events', {type:'Contest-Created', match_id: match_id})
    res.send({status:"OK"});

});



app.listen(8002, ()=>{
    console.log("listening on port 8002")
})